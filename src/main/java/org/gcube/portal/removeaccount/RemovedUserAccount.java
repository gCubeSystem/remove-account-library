package org.gcube.portal.removeaccount;

import java.util.List;

import javax.portlet.PortletPreferences;
import com.liferay.portal.service.PortalPreferencesLocalServiceUtil;

import org.gcube.common.portal.PortalContext;
import org.gcube.common.portal.mailing.EmailNotification;
import org.gcube.portal.removeaccount.Constants;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.RoleManager;
import org.gcube.vomanagement.usermanagement.UserManager;
import org.gcube.vomanagement.usermanagement.exception.RoleRetrievalFault;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.gcube.vomanagement.usermanagement.model.GatewayRolesNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;


/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class RemovedUserAccount  {

	private static final Logger _log = LoggerFactory.getLogger(RemovedUserAccount.class);

	final String SUBJECT = "User account REMOVAL notification";

	private String userName;
	private String fullName;
	private String emailAddress;
	private long userId;
	private GroupManager gm;
	private UserManager uMan;

	public RemovedUserAccount(long userId,String userName, String fullName, String emailAddress) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.fullName = fullName;
		this.emailAddress = emailAddress;
		this.uMan = new LiferayUserManager();
		this.gm = new LiferayGroupManager();
	}

	protected void doUserRemoval() {
		_log.info("trying removeUser account for " + userName);
		//first remove the account
		try {
			UserLocalServiceUtil.deleteUser(userId);
			_log.info("removeUser account for " + userName + " done with success, now notify the managers ... ");
			//set the username as reserved
			long defaultCompanyId = PortalUtil.getDefaultCompanyId();
			PortletPreferences prefs = PortalPreferencesLocalServiceUtil.getPreferences(defaultCompanyId, 1);
			String value = prefs.getValue(Constants.USERNAME_PREFERENCE_KEY, null);
			prefs.setValue(Constants.USERNAME_PREFERENCE_KEY, value+"\n"+userName);
			prefs.store();
		} catch (Exception e) {			
			e.printStackTrace();
		} 	
		//then notify the managers
		RoleManager rm = new LiferayRoleManager();
		try {
			String rootVoName = PortalContext.getConfiguration().getInfrastructureName();
			long groupId = gm.getGroupIdFromInfrastructureScope("/"+rootVoName);
			long infraManagerRoleId = -1;
			try {
				infraManagerRoleId = rm.getRoleIdByName(GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
			}
			catch (RoleRetrievalFault e) {
				_log.warn("There is no (Site) Role " + infraManagerRoleId + " in this portal. Will not notify about removed user accounts.");
				return;
			}
			_log.trace("Root is: " + rootVoName + " Scanning roles ....");	

			List<GCubeUser> managers = uMan.listUsersByGroupAndRole(groupId, infraManagerRoleId); 
			if (managers == null || managers.isEmpty()) {
				_log.warn("There are no users with (Site) Role " + infraManagerRoleId + " on " + rootVoName + " in this portal. Will not notify about removed user accounts.");
			}
			else {
				for (GCubeUser manager : managers) {
					sendNotification(manager, userName, fullName, emailAddress);
					_log.info("sent email to manager: " + manager.getEmail());	
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	private void sendNotification(GCubeUser manager, String newUserUserName, String newUserFullName, String newUserEmailAddress) {
		EmailNotification toSend = new EmailNotification(manager.getEmail(), SUBJECT, 
				getHTMLEmail(manager.getFirstName(), newUserUserName, newUserFullName, newUserEmailAddress), null);
		toSend.sendEmail();
	}

	private static String getHTMLEmail(String userFirstName, String newUserUserName, String newUserFullName, String newUserEmailAddress) {
		String sender = newUserFullName + " ("+newUserUserName+") ";

		StringBuilder body = new StringBuilder();

		body.append("<body><br />")
		.append("<div style=\"color:#000; font-size:13px; font-family:'lucida grande',tahoma,verdana,arial,sans-serif;\">")
		.append("Dear ").append(userFirstName).append(",")  //dear <user>
		.append("<p>").append(sender).append(" ").append("removed his/her account from the portal with the following email: ") // has done something
		.append(newUserEmailAddress)
		.append("</div><br />")
		.append("<p>You received this email because you are an Infrastructure Manager in this portal</p>")
		.append("</div></p>")
		.append("</body>");

		return body.toString();

	}



}
