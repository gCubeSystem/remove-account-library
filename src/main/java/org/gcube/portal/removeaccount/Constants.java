package org.gcube.portal.removeaccount;

public class Constants {
	public static final String AUTORISED_INFRA_ROLE = "Infrastructure-Manager";
	public static final String USERNAME_PREFERENCE_KEY = "admin.reserved.screen.names";

}
