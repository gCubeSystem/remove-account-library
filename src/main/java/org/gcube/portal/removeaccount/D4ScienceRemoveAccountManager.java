package org.gcube.portal.removeaccount;

import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 * This class takes care of implementing all the required actions to remove a user account from D4Science.
 */
public class D4ScienceRemoveAccountManager {
	private static final Logger _log = LoggerFactory.getLogger(D4ScienceRemoveAccountManager.class);
   	public static final String AUTORISED_INFRA_ROLE = "Infrastructure-Manager";
   	private String username2Delete;
	public D4ScienceRemoveAccountManager(String username2Delete) {
		super();
		this.username2Delete = username2Delete;
	}
   	/**
   	 * async remove user account, this method launches a series of async threads to remove all the user data from the infrastructure.
   	 * @return true is the users exists and threads are launched correctly
   	 */
   	public boolean doAsyncRemoveAccount() {	
   		GCubeUser user = null;
		try {
			user = new LiferayUserManager().getUserByUsername(username2Delete);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}   		
  	   	_log.info("Trying to remove user from Liferay DB and notify infra-managers ...");
	 	RemovedUserAccount remove = new RemovedUserAccount(
	 			user.getUserId(),
	 			user.getUsername(),
	 			user.getFullname(), 
	 			user.getEmail());
	 		remove.doUserRemoval();
       	return true;
   	}
}
